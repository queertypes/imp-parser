module Main where

import System.Environment (getArgs)
import Data.Mime.Parser (run)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [inp, outp] -> run inp outp
    _ -> putStrLn "usage: rp-unarchive <inpath> <outpath|->"