{-# LANGUAGE OverloadedStrings #-}
module Data.Mime.Parser (
  -- |Types
  Field(..),
  ContentType(..),
  Message(..),
  FromF(..),
  SubjectF(..),
  DateF(..),

  -- |Parsers
  comment,
  atom,
  quotedString,
  mailboxList,
  mailbox,
  fields,
  origDate,
  from,
  subject,
  messages,
  message,
  body,
  contBoundary,
  endBoundary,

  -- |Runners
  run
) where

import Prelude hiding (take, takeWhile, concat)
import Control.Applicative

import Data.Char
import Data.Maybe
import Data.Monoid
import Data.Text (Text, singleton, pack, concat, intercalate)
import qualified Data.Text.IO as T
import Data.Attoparsec.Text

--------------------------------------------------------------------------------
-- Core Data Types
--------------------------------------------------------------------------------

newtype Boundary = Boundary Text deriving (Show, Eq)

data Field
  = Subject Text
  | Date Text
  | From Text
  | ContentType ContentType
  | U Text Text
  deriving (Eq)

data ContentType
  = Multipart Boundary
  | Other Text
  deriving (Show, Eq)

instance Show Field where
  show (Subject x) = "Subject: " ++ show x
  show (Date x) = "Date: " ++ show x
  show (From x) = "From: " ++ show x
  show (ContentType x) = "Content-Type: " ++ show x
  show (U s c) = show s ++ ": " ++ show c

--------------------------------------------------------------------------------
-- Atoms
--------------------------------------------------------------------------------
isCtext :: Int -> Bool
isCtext n = notElem n [0, 9, 10, 13, 32, 40, 41, 92] && n <= 127

isText :: Int -> Bool
isText n = notElem n [0, 10, 13] && n <= 127

isUtext :: Int -> Bool
isUtext n = notElem n [0, 9, 10, 13, 32] && n <= 127

isAtext :: Int -> Bool
isAtext n = n > 34 && notElem n [40,41,44,46,58,59,60,62,64,91,92,93] && n <= 126

isQtext :: Int -> Bool
isQtext n = notElem n [0, 9, 13, 32, 34, 92] && n <= 127

wsp :: Char -> Bool
wsp = isHorizontalSpace

crlf :: Parser Text
crlf = choice ["\r\n" <|> "\n"]

-- |text = %d1-9 / %d11-12 / %d14-127
text :: Parser Char
text = satisfy (isText . ord)

-- |ctext = NO_WS_CTRL / %d33-39 / %d42-91 / %d93-126
ctext :: Parser Text
ctext = singleton <$> satisfy (isCtext . ord)

-- |utext = NO_WS_CTRL / %d33-126
utext :: Parser Text
utext = singleton <$> satisfy (isUtext . ord)

{-|
atext = ALPHA / DIGIT / "!" / "#" / "$" /
        "%" / "&" / "'" / "*" / "+" / "-" /
        "/" / "=" / "?" / "^" / "_" / "`" /
        "{" / "|" / "}" / "~"
-}
atext :: Parser Text
atext = singleton <$> satisfy (isAtext . ord)

-- |qtext = NO_WS_CTRL / %d33 / %d35-91 / %d93-126
qtext :: Parser Text
qtext = singleton <$> satisfy (isQtext . ord)

-- | quotedPair = ("\" text)
quotedPair :: Parser Text
quotedPair = singleton <$> ("\\" *> text)

-- |comment = "(" *([FWS] ccontent) [FWS] ")"
comment :: Parser Text
comment = concat <$> sequence ["(", concat <$> many c, optFWS *> ")"]
          where c = concat <$> sequence [optFWS, ccontent]

-- |ccontent = ctext / quotedPair / comment
ccontent :: Parser Text
ccontent = choice [ctext <|> quotedPair <|> comment]

-- |FWS = ([*WSP CRLF] 1*WSP) ; removes CRLF_s, returns space after
fws :: Parser Text
fws = concat <$> sequence [option "" optWS, pack <$> many1 wsp']
      where wsp' = satisfy wsp
            optWS = concat <$> sequence [pack <$> many wsp', crlf]


-- |CFWS = *([FWS] comment) (([FWS] comment) / FWS)
cfws :: Parser Text
cfws = skipInit *> choice [(concat <$> sequence [optFWS, comment]) <|> takeFWS]
    where skipInit = many (sequence [optFWS, comment])

-- |unstructured = *([FWS] utext) [FWS]
unstructured :: Parser Text
unstructured = concat <$> many (optFWS *> utext) <* optFWS

-- |word = atom / quoted-string
word :: Parser Text
word = choice [atom <|> quotedPair]

-- |phrase = 1*word
phrase :: Parser Text
phrase = concat <$> many1 word

-- |atom = [CFWS] 1*atext [CFWS]
atom :: Parser Text
atom = optCFWS *> (concat <$> many1 atext) <* optCFWS

-- |dotAtom = [CFWS] 1*atext *("." 1*atext) [CFWS]
dotAtom :: Parser Text
dotAtom = optCFWS *> dAtom <* optCFWS
          where dAtom = concat <$> sequence [concat <$> many1 atext, rest]
                rest = concat <$> many (concat <$> sequence [".", concat <$> many1 atext])

qcontent :: Parser Text
qcontent = choice [qtext <|> quotedPair]

quotedString :: Parser Text
quotedString = optCFWS *> qs <* optCFWS
  where qs = concat <$> sequence ["\"", m, optFWS, "\""]
        m = concat <$> many (concat <$> sequence [optFWS, qcontent])

optCFWS :: Parser Text
optCFWS = option "" cfws

optFWS :: Parser Text
optFWS = option "" fws

takeFWS :: Parser Text
takeFWS = fws

--------------------------------------------------------------------------------
-- Mailboxes and Addresses
--------------------------------------------------------------------------------

mailbox :: Parser Text
mailbox = choice [nameAddr <|> addrSpec]

nameAddr :: Parser Text
nameAddr = concat <$> sequence [option "" displayName, angleAddr]

angleAddr :: Parser Text
angleAddr = optCFWS *> (concat <$> sequence ["<", addrSpec, ">"]) <* optCFWS

displayName :: Parser Text
displayName = phrase

mailboxList :: Parser Text
mailboxList = concat <$> sequence [mailbox, rest]
  where rest = concat <$> many (concat <$> sequence [",", mailbox])

addrSpec :: Parser Text
addrSpec = concat <$> sequence [localPart, "@", domain]

localPart :: Parser Text
localPart = choice [dotAtom <|> quotedString]

domain :: Parser Text
domain = choice [dotAtom <|> domainLiteral]

domainLiteral :: Parser Text
domainLiteral = optCFWS *> dl <* optCFWS
  where dl = concat <$> sequence ["[", m, optFWS, "]"]
        m = concat <$> many (concat <$> sequence [optFWS, dcontent])

dcontent :: Parser Text
dcontent = choice [dtext <|> quotedPair]

isDtext :: Int -> Bool
isDtext n = notElem n [0, 9, 13, 32, 91, 92, 93] && n <= 127

dtext :: Parser Text
dtext = singleton <$> satisfy (isDtext . ord)

--------------------------------------------------------------------------------
-- Headers
--------------------------------------------------------------------------------

origDate :: Parser Field
origDate = Date <$> (asciiCI "Date:" *> unstructured <* crlf)

from :: Parser Field
from = From <$> (asciiCI "From:" *> takeTill isEndOfLine <* crlf)-- (asciiCI "From:" *> mailboxList <* crlf)

subject :: Parser Field
subject = Subject <$> (asciiCI "Subject:" *> unstructured <* crlf)

fieldName :: Parser Text
fieldName = concat <$> many1 ftext

ftext :: Parser Text
ftext = singleton <$> satisfy (isFtext . ord)

isFtext :: Int -> Bool
isFtext n = (n >= 33 && n <= 57) || (n >= 59 && n <= 126)

-- |Parse other headers, assume unstructured, consuming the input including final CRLF_
header :: Parser Field
header = U <$> (fieldName <* ":")
           <*> (unstructured <* crlf)

--------------------------------------------------------------------------------
-- Messages
--------------------------------------------------------------------------------

message :: Parser [Field]--, Text)
message = {-(,) <$>-} fields -- <*> (crlf *> body)

body :: Parser Text
body = concat <$> sequence [start, manyText]
  where start = concat <$> many (concat <$> sequence [manyText, crlf])
        manyText = pack <$> many text

fields :: Parser [Field]
fields = many $ choice [origDate <|> from <|> subject <|> header]

messages :: Parser [[Field]]--, Text)]
messages = many message

--------------------------------------------------------------------------------
-- Core Main
--------------------------------------------------------------------------------

newtype FromF = FromF Field deriving (Show, Eq)
newtype DateF = DateF Field deriving (Show, Eq)
newtype SubjectF = SubjectF Field deriving (Show, Eq)
data Message = Message (Maybe FromF) (Maybe SubjectF) (Maybe DateF) deriving (Show, Eq)

unarchive :: [Field] -> Maybe Message
unarchive [] = Nothing
unarchive fs = do
  let fr = FromF <$> optF findFrom fs
  let s  = SubjectF <$> optF findSubject fs
  let d  = DateF <$> optF findDate fs
  Just $ Message fr s d
  where findFrom f' = case f' of {(From _) -> True; _ -> False}
        findSubject f' = case f' of {(Subject _) -> True; _ -> False}
        findDate f' = case f' of {(Date _) -> True; _ -> False}
        optF p = listToMaybe . filter p

csvOut :: Message -> Text
csvOut ms = "num||from||subject||date\n" <> go ms (1 :: Int)
  where go (Message f s d) acc = do
          let n = pack $ show acc
          let f' = showF f
          let s' = showS s
          let d' = showD d
          let cont = intercalate "||" [n,f',s',d']
          cont <> "\n"
        showF = maybe "" (\(FromF (From x)) -> x)
        showS = maybe "" (\(SubjectF (Subject x)) -> x)
        showD = maybe "" (\(DateF (Date x)) -> x)

run :: String -> String -> IO ()
run path outp = do
  m <- parseOnly message <$> T.readFile path
  case m of
    (Left s) -> putStrLn s
    (Right as) -> do
      let ms = unarchive as
      let content = csvOut <$> ms
      case outp of
        "-"  -> T.putStrLn $ fromMaybe "" content
        out' -> T.writeFile out' $ fromMaybe "" content

--------------------------------------------------------------------------------
-- Multipart Messages
--------------------------------------------------------------------------------

contBoundary :: Text -> Parser ()
contBoundary s = "--" *> pure s *> crlf *> return ()

endBoundary :: Text -> Parser ()
endBoundary s = "--" *> pure s *> "--" *> crlf *> return ()