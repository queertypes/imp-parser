0.7.0.1 (Dec. 12, 2014)

* Clean compilation warnings
* Expose more parser bits

0.7.0.0 (Dec. 12, 2014)

* Add utility: rp-unarchive
* Made exports more specific for Parser
* Added types to support multipart parsing

0.4.0.0 (Dec. 12, 2014)

* Support parsing most RFC 2822 constructs, notably:
    * Comments and folding white space; unfolding
    * mailbox lists
    * unstructured fields
    * quoted pairs
    * quoted strings
    * Simple messages delimited by \NULs
    * From, Date, Subject fields as proper types
    * All other headers are stringly-typed
* Next release:
    * Multi-part messages

0.1.0.0 (Dec. 10, 2014)

* Initial release
